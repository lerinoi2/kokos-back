import os
import logging

from flask import Flask, request, send_from_directory, jsonify
from flask_cors import CORS
from flask_httpauth import HTTPBasicAuth
from modules import api

auth = HTTPBasicAuth()
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/')
def index():
    return app.send_static_file('fbi.html')


@app.route('/login', methods=['POST'])
def login():
    return api.login(request.json['login'], request.json['password'])


@app.route('/logout', methods=['POST'])
def logout():
    return api.logout_user(request.json['id'])


@app.route('/getinfo', methods=['POST'])
def log_org():
    return api.get_user_info(request.json['id'])


@app.route('/getusers', methods=['POST'])
def get_users():
    return api.get_prisoner(request.json['id'])


@app.route('/adduser', methods=['POST'])
def add():
    return api.add(request.json['id'], request.json['name'], request.json['surname'], request.json['lastname'],
                   request.json['date'], request.json['squad'], request.json['card'])


@app.route('/edituser', methods=['POST'])
def editusr():
    return api.edit(request.json['id'], request.json['name'], request.json['surname'], request.json['lastname'],
                    request.json['date'], request.json['squad'], request.json['card'])


@app.route('/getnumber', methods=['POST'])
def set_user_info():
    return api.get_userinfo(request.json['id'], request.json['kod'])


@app.route('/remove', methods=['POST'])
def remove():
    return api.remove(request.json['id'], request.json['kod'], request.json['phone'])


@app.route('/setnumber', methods=['POST'])
def get_user_info():
    return api.set_userinfo(request.json['id'], request.json['kod'], request.json['info'], request.json['check'])


@app.route('/removeuser', methods=['POST'])
def remove_user():
    return api.remove_user(request.json['id'], request.json['kod'])


if __name__ == '__main__':
    app.run(host='bot.delive.me', port=8001, debug=True)
