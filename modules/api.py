from flask import jsonify

from modules import loginCheck, getInfo, \
    getUsers, setUserInfo, getUserInfo, logout, removePhone, \
    addUser, editUser, removeUser


def check_return(result):
    return jsonify(result)


def login(log, password):
    result = loginCheck.login(log, password)
    return check_return(result)


def logout_user(session_id):
    result = logout.out(session_id)
    return check_return(result)


def get_user_info(session_id):
    result = getInfo.login_id(session_id)
    return check_return(result)


def get_prisoner(session_id):
    result = getUsers.get(session_id)
    return check_return(result)


def add(session_id, name, surname, lastname, date, squad, card):
    result = addUser.add(session_id, name, surname, lastname, date, squad, card)
    return check_return(result)


def edit(session_id, name, surname, lastname, date, squad, card):
    result = editUser.edit(session_id, name, surname, lastname, date, squad, card)
    return check_return(result)


def get_userinfo(session_id, kod):
    result = getUserInfo.set(session_id, kod)
    return check_return(result)


def remove(session_id, kod, phone):
    result = removePhone.remove(session_id, kod, phone)
    return check_return(result)


def set_userinfo(session_id, kod, info, check):
    result = setUserInfo.set(session_id, kod, info, check)
    return check_return(result)


def remove_user(session_id, kod):
    result = removeUser.delete(session_id, kod)
    return check_return(result)
