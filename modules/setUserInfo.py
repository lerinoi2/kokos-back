from connect import methods


def set(session_id, kod, info, check):
    query = """select AGENT from AUTORIZ where """
    query += """sessionId = '""" + session_id + "'"
    agent_id = methods.query(query)[0][0]
    if not agent_id:
        return {'error': '5'}
    else:
        if not check:
            sql = """update PRISONER set WHITE_LIST = 0 WHERE CARD = '""" + kod + "'"
            methods.insert_date(sql)
            if not info:
                return {'return': 'success'}
        else:
            sql = """update PRISONER set WHITE_LIST = 1 WHERE CARD = '""" + kod + "'"
            methods.insert_date(sql)
            if not info:
                return {'return': 'success'}
        if isinstance(info, list):
            for val in info:
                res = set_db(val, kod)
            return res
        else:
            return set_db(info, kod)

def set_db(info, kod):
    kod = kod.replace(' ', '')
    phone = str(info[1:])
    sql = """insert into PRISONER_NUMBERS (CARD, "NUMBER") values(:1, :2)"""
    res = methods.insert(sql, (kod, phone))
    if res:
        return {'return': 'success'}
    else:
        return {'error': '8'}
