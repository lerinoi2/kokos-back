from datetime import datetime
from connect import methods
import json


def add(session_id, name, surname, lastname, date, squad, card):
    query = """select AGENT from AUTORIZ where """
    query += """sessionId = '""" + session_id + "'"
    agent = methods.query(query)[0][0]
    if not agent:
        return {'error': '5'}
    else:
        query = """insert into PRISONER (AGENT,FAMILY,NAME,SURNAME,DATE_BIRTH,SQUAD,CARD)
                VALUES('""" + agent + """',
                        '""" + lastname + """',
                        '""" + name + """',
                        '""" + surname + """',
                        TO_DATE('""" + date + """', 'YYYY-MM-DD HH24:MI:SS'),
                        '""" + squad + """',
                        '""" + card + """')"""
        res = methods.insert_date(query)
        if res:
            return {'return': 'success'}
        else:
            return {'error': '32'}
