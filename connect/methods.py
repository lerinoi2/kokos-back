from connect import config
curs = config.connection.cursor()


def query(sql):
    try:
        curs.execute(sql)
        row = curs.fetchall()
        return row
    except config.cx_Oracle.DatabaseError:
        return False


def insert(sql, dataArray):
    try:
        curs.execute(sql, dataArray)
        config.connection.commit()
        return True
    except config.cx_Oracle.DatabaseError:
        return False


def insert_date(sql):
    try:
        curs.execute(sql)
        config.connection.commit()
        return True
    except config.cx_Oracle.DatabaseError:
        return False
